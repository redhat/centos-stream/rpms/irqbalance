From d16ad5df4cd14b148513ab9d5cf0ebcf09d023e0 Mon Sep 17 00:00:00 2001
From: Andrew Zaborowski <andrew.zaborowski@intel.com>
Date: Fri, 10 May 2024 18:38:52 -0700
Subject: [PATCH 22/44] Wrap migrate_irq in a higher level utility function

Add migrate_irq_obj and replace existing migrate_irq calls with calls to
the new function.  migrate_irq_obj takes source and destination
topo_obj's instead of interrupt lists so as to factor out updating of
the load on the destination cpu and of info->asssigned_obj.

Pass NULL as destination to move irq to rebalance_irq_list.

Drop the unneeded force_irq_migration.
---
 irqbalance.c |  4 +---
 irqbalance.h |  1 +
 irqlist.c    | 31 +++++++++++++++++++++----------
 placement.c  | 24 +++++-------------------
 4 files changed, 28 insertions(+), 32 deletions(-)

diff --git a/irqbalance.c b/irqbalance.c
index 870d7c0..7efbc98 100644
--- a/irqbalance.c
+++ b/irqbalance.c
@@ -253,9 +253,7 @@ void force_rebalance_irq(struct irq_info *info, void *data __attribute__((unused
 	if (info->assigned_obj == NULL)
 		rebalance_irq_list = g_list_append(rebalance_irq_list, info);
 	else
-		migrate_irq(&info->assigned_obj->interrupts, &rebalance_irq_list, info);
-
-	info->assigned_obj = NULL;
+		migrate_irq_obj(info->assigned_obj, NULL, info);
 }
 
 gboolean handler(gpointer data __attribute__((unused)))
diff --git a/irqbalance.h b/irqbalance.h
index 09daa3d..76640dd 100644
--- a/irqbalance.h
+++ b/irqbalance.h
@@ -52,6 +52,7 @@ void dump_workloads(void);
 void sort_irq_list(GList **list);
 void calculate_placement(void);
 void dump_tree(void);
+void migrate_irq_obj(struct topo_obj *from, struct topo_obj *to, struct irq_info *info);
 
 void activate_mappings(void);
 void clear_cpu_tree(void);
diff --git a/irqlist.c b/irqlist.c
index 0ba411e..304b1c6 100644
--- a/irqlist.c
+++ b/irqlist.c
@@ -108,9 +108,7 @@ static void move_candidate_irqs(struct irq_info *info, void *data)
 
 	log(TO_CONSOLE, LOG_INFO, "Selecting irq %d for rebalancing\n", info->irq);
 
-	migrate_irq(&info->assigned_obj->interrupts, &rebalance_irq_list, info);
-
-	info->assigned_obj = NULL;
+	force_rebalance_irq(info, NULL);
 }
 
 static void migrate_overloaded_irqs(struct topo_obj *obj, void *data)
@@ -146,12 +144,6 @@ static void migrate_overloaded_irqs(struct topo_obj *obj, void *data)
 	}
 }
 
-static void force_irq_migration(struct irq_info *info, void *data __attribute__((unused)))
-{
-	migrate_irq(&info->assigned_obj->interrupts, &rebalance_irq_list, info);
-	info->assigned_obj = NULL;
-}
-
 static void clear_powersave_mode(struct topo_obj *obj, void *data __attribute__((unused)))
 {
 	obj->powersave_mode = 0;
@@ -183,7 +175,7 @@ void update_migration_status(void)
 			log(TO_ALL, LOG_INFO, "cpu %d entering powersave mode\n", info.powersave->number);
 			info.powersave->powersave_mode = 1;
 			if (g_list_length(info.powersave->interrupts) > 0)
-				for_each_irq(info.powersave->interrupts, force_irq_migration, NULL);
+				for_each_irq(info.powersave->interrupts, force_rebalance_irq, NULL);
 		} else if ((info.num_over) && (info.num_powersave)) {
 			log(TO_ALL, LOG_INFO, "Load average increasing, re-enabling all cpus for irq balancing\n");
 			for_each_object(cpus, clear_powersave_mode, NULL);
@@ -205,3 +197,22 @@ void dump_workloads(void)
 	for_each_irq(NULL, dump_workload, NULL);
 }
 
+void migrate_irq_obj(struct topo_obj *from, struct topo_obj *to, struct irq_info *info)
+{
+
+	GList **from_list;
+	GList **to_list;
+
+	if (!from)
+		from = info->assigned_obj;
+
+	from_list = from ? &from->interrupts : &rebalance_irq_list;
+	to_list = to ? &to->interrupts : &rebalance_irq_list;
+
+	migrate_irq(from_list, to_list, info);
+
+	if (to)
+		to->load += info->load + 1;
+
+	info->assigned_obj = to;
+}
diff --git a/placement.c b/placement.c
index dea7c23..f156e0e 100644
--- a/placement.c
+++ b/placement.c
@@ -74,7 +74,6 @@ static void find_best_object_for_irq(struct irq_info *info, void *data)
 {
 	struct obj_placement place;
 	struct topo_obj *d = data;
-	struct topo_obj *asign;
 
 	if (!info->moved)
 		return;
@@ -107,13 +106,8 @@ static void find_best_object_for_irq(struct irq_info *info, void *data)
 
 	for_each_object(d->children, find_best_object, &place);
 
-	asign = place.best;
-
-	if (asign) {
-		migrate_irq(&d->interrupts, &asign->interrupts, info);
-		info->assigned_obj = asign;
-		asign->load += info->load;
-	}
+	if (place.best)
+		migrate_irq_obj(d, place.best, info);
 }
 
 static void place_irq_in_object(struct topo_obj *d, void *data __attribute__((unused)))
@@ -125,7 +119,6 @@ static void place_irq_in_object(struct topo_obj *d, void *data __attribute__((un
 static void place_irq_in_node(struct irq_info *info, void *data __attribute__((unused)))
 {
 	struct obj_placement place;
-	struct topo_obj *asign;
 
 	if ((info->level == BALANCE_NONE) && cpus_empty(banned_cpus))
 		return;
@@ -145,9 +138,7 @@ static void place_irq_in_node(struct irq_info *info, void *data __attribute__((u
 		 * This irq belongs to a device with a preferred numa node
 		 * put it on that node
 		 */
-		migrate_irq(&rebalance_irq_list, &irq_numa_node(info)->interrupts, info);
-		info->assigned_obj = irq_numa_node(info);
-		irq_numa_node(info)->load += info->load + 1;
+		migrate_irq_obj(NULL, irq_numa_node(info), info);
 
 		return;
 	}
@@ -159,13 +150,8 @@ find_placement:
 
 	for_each_object(numa_nodes, find_best_object, &place);
 
-	asign = place.best;
-
-	if (asign) {
-		migrate_irq(&rebalance_irq_list, &asign->interrupts, info);
-		info->assigned_obj = asign;
-		asign->load += info->load;
-	}
+	if (place.best)
+		migrate_irq_obj(NULL, place.best, info);
 }
 
 static void validate_irq(struct irq_info *info, void *data)
-- 
2.47.0

